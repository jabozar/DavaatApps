package com.daavatapps.abozar.toolbar;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class ShowCards extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_cards);

        TextView TV = (TextView) findViewById(R.id.txtTitleApp);
        TV.setText(Html.fromHtml(getString(R.string.matn)));
        TV.setMovementMethod(LinkMovementMethod.getInstance());
        Typeface TF = Typeface.createFromAsset(getAssets(), "fonts/isans.ttf");
        TV.setTypeface(TF);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //1-way 1
                // Toast.makeText(getBaseContext(), "جهت اطلاعات بیشتر به سایت دوات اپس مراجعه بفرمایید" ,Toast.LENGTH_SHORT).show();

                //2- way2
                /*Snackbar.make(v , "جهت مشاهده نمونه آپلیکیشن ها به سایت مراجعه بفرماید",Snackbar.LENGTH_LONG).setAction("ورود به سایت دوات", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://davaatapps.com/"));
                        startActivity(intent);
                    }
                }).show();*/

                //3- way3
                Snackbar snackbar = Snackbar.make(view, "مشاهده نمونه اپلیکیشن ها", Snackbar.LENGTH_LONG);
                View v = snackbar.getView();
                v.setBackgroundColor(getResources().getColor(R.color.snackbar_background));
                TextView tv = (TextView) v.findViewById(android.support.design.R.id.snackbar_text); // jahate avaz kardane range text snack bar
                tv.setTextColor(getResources().getColor(R.color.snackbar_text));
                Typeface tfSnackText = Typeface.createFromAsset(getAssets(), "fonts/isans.ttf");
                tv.setTypeface(tfSnackText);

                snackbar.setAction("ورود به سایت", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse("http://davaatapps.com/"));
                        startActivity(intent);

                    }
                });
                snackbar.show();
            }
        });

    }
}
