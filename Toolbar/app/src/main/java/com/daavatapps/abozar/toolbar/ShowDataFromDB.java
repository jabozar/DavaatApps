package com.daavatapps.abozar.toolbar;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Abozar on 12/20/2016.
 */

public class ShowDataFromDB extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_showdata_fromdb);

        TextView tv_ShowData = (TextView) findViewById(R.id.tvShowData);

        Database db = new Database(this);

        try {
            db.open();

            String getData = db.getDataFromDB();

            db.close();
            tv_ShowData.setText(getData);
        }catch (Exception e){
            String err = e.toString();
            Toast.makeText(getApplicationContext() , err , Toast.LENGTH_LONG).show();
        }


    }
}
