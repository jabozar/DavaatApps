package com.daavatapps.abozar.toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Abozar on 12/17/2016.
 */

public class Register extends AppCompatActivity {

    EditText et_FirstName, et_LastName, et_NickName, et_Password, et_ConfirmPassword, et_Email, et_MobileNumber;
    Button btn_Preview, btn_Save, btn_Return;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);


        init();

        btn_Save.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            String FName = et_FirstName.getText().toString();
                                            String LName = et_LastName.getText().toString();
                                            String NName = et_NickName.getText().toString();
                                            String pass = et_Password.getText().toString();
                                            String cPass = et_ConfirmPassword.getText().toString();
                                            String email = et_Email.getText().toString();
                                            String mobile = et_MobileNumber.getText().toString();


                                            if (FName.isEmpty() || LName.isEmpty() || NName.isEmpty() || pass.isEmpty() || cPass.isEmpty() || email.isEmpty() || mobile.isEmpty()) {
                                                Toast.makeText(getApplicationContext(), "لطفا فیلدهای مربوطه را کامل بفرمایید", Toast.LENGTH_SHORT).show();

                                            } else if (validate(pass, cPass, email, mobile) == true) {

                                                Database database = new Database(Register.this);
                                                try {

                                                    database.open();

                                                    database.makeEntry(FName, LName, NName, pass, cPass, email, mobile);

                                                    database.close();

                                                } catch (Exception e) {
                                                    String err = e.toString();
                                                    Toast.makeText(getApplicationContext(), err, Toast.LENGTH_LONG).show();
                                                } finally {
                                                    Toast.makeText(getApplicationContext(), "نام کاربری شما با موفقیت ثبت گردید", Toast.LENGTH_LONG).show();
                                                }

                                            }
                                        }

                                    }
        );
        btn_Preview.setOnClickListener(new View.OnClickListener()

                                       {
                                           @Override
                                           public void onClick(View v) {
                                               Intent intent = new Intent(Register.this , ShowDataFromDB.class);
                                               startActivity(intent);

                                           }
                                       }

        );

        btn_Return.setOnClickListener(new View.OnClickListener()

                                      {
                                          @Override
                                          public void onClick(View v) {
                                              Intent intentReturn = new Intent(Register.this, Login.class);
                                              startActivity(intentReturn);
                                          }
                                      }

        );

    }

    private void init() {
        et_FirstName = (EditText) findViewById(R.id.etFirstName);
        et_LastName = (EditText) findViewById(R.id.etLastName);
        et_NickName = (EditText) findViewById(R.id.etNickName);
        et_Password = (EditText) findViewById(R.id.etPassword);
        et_ConfirmPassword = (EditText) findViewById(R.id.etConfirmPassword);
        et_Email = (EditText) findViewById(R.id.etEmail);
        et_MobileNumber = (EditText) findViewById(R.id.etMobileNumber);
        btn_Preview = (Button) findViewById(R.id.btnPreview);
        btn_Save = (Button) findViewById(R.id.btnSave);
        btn_Return = (Button) findViewById(R.id.btnReturn);
    }

    private boolean validate(String p, String cp, String em, String mob) {
        String validEmailPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        if (p.length() < 6) {

            et_Password.setError("رمز عبور باید حداقل 6 رقم باشد");
        } else if (!p.equals(cp)) {
            et_ConfirmPassword.setError("رمز عبور وارد شده یکسان نمی باشد");
        } else if (!em.equals(validEmailPattern)) {
            et_Email.setError("آدرس ایمیل معتبر نمی باشد");
        } else if (mob.length() < 11) {
            et_MobileNumber.setError("شماره موبایل باید 11 رقم باشد");
        } else {

        }
        return true;

    }


    @Override
    protected void onPause() {
        super.onPause();
        finish();

    }
}
